<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Datatables;
use Excel;
use Mail;
use Carbon\Carbon;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $start = DB::table('campaign')->where('key', 'start_time')->value('value');
        $end = DB::table('campaign')->where('key', 'end_time')->value('value');
        $campaign_time['start'] = $start;
        $campaign_time['end'] = $end;
        $social = [
            'facebook' => DB::table('clicks')->where('location', 'facebook')->count(),
        ];

        return view('admin',['campaign_time' => $campaign_time, 'social' => $social]) ; 

    }

    public function saveDate(Request $request)
    {
        DB::table('campaign')
            ->where('key', 'start_time')
            ->update(['value' => $request->start]);
        DB::table('campaign')
            ->where('key', 'end_time')
            ->update(['value' => $request->end]);
        $response = array(
            'status' => 'success',
            'msg' => 'Date saved.',
            'start' => $request->start,
            'end' => $request->end,
        );
        return response()->json($response);
    }

    public function generateUniqueCodes($amount)
    {
        $allowedChars = 'BCDFGHJKLMNPQRSTVWXYZ0123456789';

        for ($i = 0; $i <= $amount; $i++) {
            $code = '';

            for ($j = 0; $j < 5; $j++) {
                $code .= $allowedChars[mt_rand(0, strlen($allowedChars) - 1)];
            }

            $existed = DB::table('codes')->where('code', $code)->count();

            if (!$existed) {
                DB::table('codes')->insert([
                    'code' => $code,
                    'used' => false,
                ]);
            }
        }

        return $amount . ' codes generated <button onclick="window.close()">Close</button>';
    }

    public function exportCodeList()
    {
        $fileName = 'Laura_mercier_(Code list)';

        Excel::create($fileName, function ($excel) {
            // left align all rows

            $excel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $excel->sheet('codes', function ($sheet) {
                $rowIndex = 1;

                $sheet->row($rowIndex, [
                    'No.',
                    'Code',
                ]);

                $codes = DB::table('codes')->orderBy('id', 'asc')->get();

                foreach ($codes as $key => $value) {
                    $rowIndex++;

                    $sheet->row($rowIndex, [
                        $value->id,
                        $value->code,
                    ]);
                }

                $sheet->setAutoSize(true);
            });
        })->download('xlsx');
    }

    public function getRegistration(Request $request)
    {
        $registration = DB::table('registrations')
            ->groupBy( DB::raw('DATE(created_at)') )
            ->orderBy( DB::raw('DATE(created_at)'), 'desc' )
            ->select(
                DB::raw('DATE(created_at) AS date'),
                DB::raw('COUNT(*) AS registration_count')
            );

        if ($request->date) {
            $date = date( 'Y-m-d', strtotime($request->date) );

            $registration->where( DB::raw('DATE(created_at)'), $date );
        }

        return Datatables::of($registration)
            ->make(true);
    }
    /**
     * Return customers as JSON data.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCustomers(Request $request)
    {
        $customers = DB::table('registrations')
            ->orderBy( DB::raw('DATE(created_at)'), 'desc' )
            ->select(
                'created_at',
                'fullname',
                'email',
                'redeem_location',
                'unique_code as verification_code'
            );

        if ($request->date) {
            $date = date( 'Y-m-d', strtotime($request->date) );

            $customers->where( DB::raw('DATE(created_at)'), $date );
        }

        return Datatables::of($customers)->make(true);
    }

    /**
     * Return download of customer data as XLSX file.
     *
     * @return \Illuminate\Http\Response
     */
    public function exportExcel($date = null)
    {
        $fileName = 'Laura_mercier_';

        $fileName .= ($date)?
            'on_' . date( 'Y_m_d', strtotime($date) ):
            'up_to_' . date('Y_m_d_h_i_s');

        Excel::create($fileName, function ($excel) use ($date) {

            // set all row align left
            $excel->getDefaultStyle()
                ->getAlignment()
                ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);

            $excel->sheet('registrations', function ($sheet) use ($date) {
                $rowIndex = 1;

                $sheet->row($rowIndex, [
                    'Register Date',
                    'Full Name',
                    'Email Address',
                    'Selected Redeem Outlet',
                    'Redeem Code',
                ]);

                $reservationModel = DB::table('registrations');

                if ($date) {
                    $reservationModel->whereDate('created_at', $date);
                }

                $reservations = $reservationModel->get();

                foreach ($reservations as $key => $value) {
                    $rowIndex++;

                    $sheet->row($rowIndex, [
                        date( 'j M Y', strtotime($value->created_at) ),
                        $value->fullname,
                        $value->email,
                        $value->redeem_location,
                        $value->unique_code
                    ]);
                }

                $sheet->setAutoSize(true);
            });
        })->download('xls');
    }
}

<?php

namespace App\Http\Controllers;

use DB;
use Mail;
use Validator;
use Illuminate\Http\Request;
use App\Mail\RegistrationVerified;
use Carbon\Carbon;

class HomeController extends Controller
{
    #region Active Period
    public function index()
    {
        $end = DB::table('campaign')->where('key', 'end_time')->value('value');
        if ( Carbon::parse($end)->isFuture() ) {
            return view('main');
        } else {
            return view('end', ['end'=>$end] );
        }
    }
    #endregion

    #region Submit Form
    public function submit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fullname' => 'required|max:255',
    		'email' => 'required|email|max:255|unique:registrations,email',
            'location' => 'required',
        ]);

        if ( $validator->fails() ) {
            return back()->withErrors($validator)->withInput();
        }

         // Get code from code table

         $codeObj = DB::table('codes')->where('used', false)->first();
         if ($codeObj){
             $code = $codeObj->code;
             DB::table('codes')->where('code', $code)->update(['used' => true]);
         }
         else{ // handle when all code used.
             $code = $this->generateUniqueCode();
             DB::table('codes')->insert([
                 'code' => $code,
                 'used' => true,
             ]);
         }

        // Save new registration to database

        $registrationId = DB::table('registrations')->insertGetId([
            'fullname' => $request->fullname,
            'email' => $request->email,
            'redeem_location' => $request->location,
            'unique_code' => $code,
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        // Send email

        Mail::to([$request->fullname => $request->email])
             ->send(new RegistrationVerified($request, $code));

        return redirect()->route('thank-you', [
            'unique_code' => $code,
        ]);
    }
    #endregion

    #region ThankYou
    public function thankYou($code)
    {
        $registration = DB::table('registrations')->where('unique_code', $code)->first();
        
        if ($registration) {
            return view('thank-you', ['unique_code' => $code,'registration'=>$registration]);
        } else {
            abort(404);
        }
    }
    #endregion

    public function generateUniqueCode()
    {
        $codeCharacters = 'BCDFGHJKLMNPQRSTVWXYZ0123456789';
        while (true) {
            $code = '';
            for ($i = 0; $i < 5; $i++) {
                $code .= $codeCharacters[mt_rand(0, strlen($codeCharacters) - 1)];
            }

            // Check if the generated code exists
            $existed = DB::table('registrations')->where('unique_code', $code)->count();

            if (!$existed) {
                break;
            }
        }
        return $code;
    }

    #region Redirect
    public function redirect($location)
    {
        DB::table('clicks')->insert([
            'location' => $location,
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        switch ($location) {
            case 'facebook':
                $url = 'https://www.facebook.com/lauramerciercosmeticsmalaysia/';
                break;
        }
        return redirect($url);
    }
    #endregion
}

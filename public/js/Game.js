Candy.Game = function(game){
	// define needed variables for Candy.Game
	this._player = null;
	this._candyGroup = null;
	this._bombGroup = null;
	this._spawnCandyTimer = 0;
	this._spawnBombTimer = 0;
	this._fontStyle = null;
	// define Candy variables to reuse them in Candy.item functions
	Candy._scoreText = null;
	//Candy._levelText = null;
	Candy._score = 0;
	Candy._health = 0;
	Candy._healthFeedback = null;
};
Candy.Game.prototype = {
	create: function(){
		// AJAX for starting game
		$.post('/game-start');
		// start the physics engine
		this.physics.startSystem(Phaser.Physics.ARCADE);
		// set the global gravity
		this.physics.arcade.gravity.y = 400;
		// display images: background, floor and score
		this.add.sprite(0, 0, 'landing-background');
		this.add.sprite(0, Candy.GAME_HEIGHT-300, 'floor');
		this.add.sprite(10, 5, 'score-bg');
		// add pause button
		this.add.button(Candy.GAME_WIDTH-80, 5, 'button-pause', this.managePause, this);
		// create the player
		this._player = this.add.sprite(60, 570, 'monster-idle');

		// add player animation
		//this._player.animations.add('idle', [0,1,2,3,4,5,6,7,8,9,10,11,12], 12, true);
		// play the animation
		//this._player.animations.play('idle');

		// set font style
		this._fontStyle = { font: "40px Arial", fill: "#FFCC00", stroke: "#333", strokeThickness: 5, align: "center" };
		// initialize the spawn timer
		this._spawnCandyTimer = 0;
		this._spawnBombTimer = 0;
		// initialize the score text with 0
		Candy._scoreText = this.add.text(100, 20, "0", this._fontStyle);
		//Candy._levelText = this.add.text(500, 100, "0", this._fontStyle);

		// set health of the player
		Candy._health = 3;
		Candy._headthFeedback = this.add.sprite(10, Candy.GAME_HEIGHT-80, 'life');
		// create new group for candy
		this._candyGroup = this.add.group();
		// create new group for bomb
		this._bombGroup = this.add.group();
		// spawn first candy
		Candy.item.spawnCandy(this);
	},
	managePause: function(){
		// pause the game
		this.game.paused = true;
		// add proper informational text
		var pausedText = this.add.text(100, 250, "Game paused.\nTap anywhere to continue.", this._fontStyle);
		// set event listener for the user's click/tap the screen
		this.input.onDown.add(function(){
			// remove the pause text
			pausedText.destroy();
			// unpause the game
			this.game.paused = false;
		}, this);
	},
	update: function(){
		// update timer every frame
		this._spawnCandyTimer += this.time.elapsed;
		this._spawnBombTimer += this.time.elapsed;
		var spawnTimer;

		if (Candy._score > 50) {
			spawnTimer = 600;
		} else if (Candy._score > 100) {
			spawnTimer = 500;
		} else if (Candy._score > 150) {
			spawnTimer = 400;
		} else if (Candy._score > 200) {
			spawnTimer = 300;
		} else {
			spawnTimer = 700;
		}

		// if spawn candy timer reach 0.7 second (700 miliseconds) with increasing difficulty
		if(this._spawnCandyTimer > spawnTimer) {
			// reset it
			this._spawnCandyTimer = 0;
			// and spawn new candy
			Candy.item.spawnCandy(this);
		}
		// if spawn bomb timer reach 3.1 second (3100 miliseconds)
		if(this._spawnBombTimer > (spawnTimer*4.3)) {
			// reset it
			this._spawnBombTimer = 0;
			// and spawn new bomb
			Candy.item.spawnBomb(this);

			this.physics.arcade.gravity.y += 20;

			//Candy._level += 1;
			//
			//if(this.physics.arcade.gravity.y + 60) {
			//
			//	Candy._levelText.setText('1');
			//
			//} else if (this.physics.arcade.gravity.y == 420) {
			//
			//	Candy._levelText.setText(Candy._level);
			//
			//}

		}

		// loop through all candy on the screen
		this._candyGroup.forEach(function(candy){
			// to rotate them accordingly
			candy.angle += candy.rotateMe;
		});
		// loop through all bomb on the screen
		this._bombGroup.forEach(function(bomb){
			// to rotate them accordingly
			bomb.angle += bomb.rotateMe;
		});

		//if the health of the player drops to 0, the player dies = game over
		if(!Candy._health) {
			// play sound for game over
			gamesound_over.play();
			// print health
			$('.score').text(Candy._score);

			// popup registration form with score
			if (Candy._score > 0) {
				var scoreText = 'You have earned <span class="lead">' + Candy._score + '</span> score!';
			} else {
				var scoreText = 'Don\'t give up! <span class="lead">try again?</span>';
			}

			$('#registration_form').modal({
				backdrop: 'static'
			});

			// AJAX for submit score
			$.post('/game-end', {
				data: {
					score: Candy._score
				}
			}).fail(function () {
				alert('Oops! The connection to server is lost and we are unable to save your score.');
				$('#registration_form').modal('hide');
			});

			// Health feedback reach zero
			Candy._headthFeedback.frame = 3;

			// show the game over message
			this.add.sprite((Candy.GAME_WIDTH-405)/2, (Candy.GAME_HEIGHT-580)/2, 'game-over');

			// pause the game
			this.game.paused = true;

			//restart game text
			var reloadText = this.add.text(100, 520, "Tap anywhere to restart.", this._fontStyle);

			//touch the screen to restart game
			this.input.onDown.add(function(){
				Candy._score = 0;
				this.state.start('Game');
				this.game.paused = false;
				Candy._headthFeedback.frame = 0;
				reloadText.destroy();
			}, this);

			//jump to gameover screen and show restart button
			//this.game.world.removeAll();
			//this.add.sprite((Candy.GAME_WIDTH-594)/2, (Candy.GAME_HEIGHT-271)/2, 'game-over');
			//this.add.button(Candy.GAME_WIDTH-501-10, Candy.GAME_HEIGHT-200-10, 'button-start', this.reloadGame , this, 1, 0, 2);
			//this.game.paused = false;

		} else {
			switch (Candy._health) {
				case 1:
					Candy._headthFeedback.frame = 2;
					break;
				case 2:
					Candy._headthFeedback.frame = 1;
					break;
			}
		}
	},
	reloadGame: function() {
		// reload the Game state
		this.state.start('Game');
	}
};

Candy.item = {
	spawnCandy: function(game){
		// calculate drop position (from 0 to game width) on the x axis
		//var dropPos = Math.floor(Math.random()*Candy.GAME_WIDTH);
		var dropPos = Math.floor((Math.random()*500)+90);
		// define the offset for every candy
		var dropOffset = [-30,-32,-39,-25,-29];
		// randomize candy type
		var candyType = Math.floor(Math.random()*5);
		// create new candy
		var candy = game.add.sprite(dropPos, dropOffset[candyType], 'candy');
		// create burst effect
		candy.burst = game.add.emitter(0, 0, 3);
		candy.burst.makeParticles('star-effect');
		candy.burst.gravity = -500;
		// add new animation frame
		candy.animations.add('anim', [candyType], 10, true);
		// play the newly created animation
		candy.animations.play('anim');
		// enable candy body for physic engine
		game.physics.enable(candy, Phaser.Physics.ARCADE);
		// enable candy to be clicked/tapped
		candy.inputEnabled = true;
		// add event listener to click/tap
		candy.events.onInputDown.add(this.clickCandy, this);
		// be sure that the candy will fire an event when it goes out of the screen
		candy.checkWorldBounds = true;
		// reset candy when it goes out of screen
		candy.events.onOutOfBounds.add(this.removeCandy, this);
		// set the anchor (for rotation, position etc) to the middle of the candy
		candy.anchor.setTo(0.5, 0.5);
		// set the random rotation value
		candy.rotateMe = (Math.random()*4)-2;
		// add candy to the group
		game._candyGroup.add(candy);
	},
	clickCandy: function(candy){
		// kill the candy when it's clicked
		candy.kill();
		// play sound for scoring
		gamesound_score.play();
		// run burst effect
		candy.burst.x = candy._bounds.x + (candy._bounds.width/2);
		candy.burst.y = candy._bounds.y + (candy._bounds.height/2);
		candy.burst.start(true, 1000, null, 3);
		// add points to the score
		Candy._score += 1;
		// update score text
		Candy._scoreText.setText(Candy._score);
	},
	removeCandy: function(candy){
		// kill the candy
		candy.kill();
		// play sound for missing
		gamesound_missed.play();
		// decrease player's health
		Candy._health -= 1;
	},
	spawnBomb: function(game) {
		// calculate drop position (from 0 to game width) on the x axis
		var dropPos = Math.floor(Math.random()*Candy.GAME_WIDTH);
		// create new bomb
		var bomb = game.add.sprite(dropPos, -49, 'bomb');
		// create explosion effect
		bomb.explode = game.add.emitter(0, 0, 3);
		bomb.explode.makeParticles('explosion');
		bomb.explode.gravity = -500;
		// enable bomb body for physic engine
		game.physics.enable(bomb, Phaser.Physics.ARCADE);
		// enable candy to be clicked/tapped
		bomb.inputEnabled = true;
		// add event listener to click/tap
		bomb.events.onInputDown.add(this.clickBomb, this);
		// be sure that the bomb will fire an event when it goes out of the screen
		bomb.checkWorldBounds = true;
		// reset bomb when it goes out of screen
		bomb.events.onOutOfBounds.add(this.removeBomb, this);
		// set the anchor (for rotation, position etc) to the middle of the bomb
		bomb.anchor.setTo(0.5, 0.5);
		// set the random rotation value
		bomb.rotateMe = (Math.random()*4)-2;
		// add bomb to the group
		game._bombGroup.add(bomb);
	},
	clickBomb: function(bomb) {
		// kill the bomb when it's clicked
		bomb.kill();
		// play sound for missing
		gamesound_bomb.play();
		// run explosion effect
		bomb.explode.x = bomb._bounds.x + (bomb._bounds.width/2);
		bomb.explode.y = bomb._bounds.y + (bomb._bounds.height/2);
		bomb.explode.start(true, 500, null, 3);
		// decrease player's health
		Candy._health -= 1;
	},
	removeBomb: function(bomb) {
		// kill the bomb
		bomb.kill();
	}
};

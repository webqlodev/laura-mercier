// preload quiz banner image
var background_banner = {
    'q2a':"../img/quiz-banner2a.jpg",
    'q2b':"../img/quiz-banner2b.jpg",
    'q3a':"../img/quiz-banner3a.jpg",
    'q3b':"../img/quiz-banner3b.jpg",
    'ansA':"../img/answer1.jpg",
    'ansB':"../img/answer2.jpg",
    'ansC':"../img/answer3.jpg",
    'ansD':"../img/answer4.jpg",
};

$(function(){
    for (var banner in background_banner){
        $('.preload-image').append("<img src='"+background_banner[banner]+"' />");
        console.log(banner);
    }
});

// start the quiz
function qStart() {
    $('.cleanup').fadeOut(300, function() {
        $(this).empty().append(
            '<div class="head2 white">Indoors or Outdoors?</div>' +
            '<a style="padding:10px 40px;" class="btn btn-q btn-qa" data-action="q2a">The bedside is my side</a>' +
            '<a class="bspace"></a>' +
            '<a style="padding:10px 30px;" class="btn btn-q btn-qb" data-action="q2b">Outside Is where I\'ll stride</a>' +
            '<div class="mtop-50"><span class="indicators active">1</span><span class="ispace"></span><span class="indicators">2</span><span class="ispace"></span><span class="indicators">3</span></div>'
        ).fadeIn(300);
        refresh();
    });
}

//refresh page load
function refresh(){
    $('.btn-q').off('click',onClick);
    $('.btn-q').on('click',onClick);
}

//button action lead to
function onClick(){
    var action = $(this).data('action');
    if(action == 'start'){
        qStart();
    } else if (action == 'q2a'){
        q2aClick();
    } else if (action == 'q2b'){
        q2bClick();
    } else if (action == 'q3a'){
        q3aClick();
    } else if (action == 'q3b'){
        q3bClick();
    } else if (action == 'ansA'){
        ansAClick();
    } else if (action == 'ansB'){
        ansBClick();
    } else if (action == 'ansC'){
        ansCClick();
    } else if (action == 'ansD'){
        ansDClick();
    }
}

// quiz 2 answer A click
function q2aClick() {
    $('.cleanup').fadeOut(300, function() {
        if ($(window).width() > 550) {
            $('.quiz-banner').css('background-image','url('+background_banner['q2a']+')');
        } else {
            $('.quiz-banner').css('background-image','url('+background_banner['q2a']+')');
            $('.quiz-banner').css('background-position','30% 80%');
        }

        $(this).empty().append(
            '<div class="head2 white">Bliss feels like walking down?</div>' +
            '<a style="padding:10px 40px;" class="btn btn-q btn-qa" data-action="q3a">The concrete jungle!</a>' +
            '<a class="bspace"></a>' +
            '<a style="padding:10px 40px;" class="btn btn-q btn-qb" data-action="q3b">Solemn country roads</a>' +
            '<div class="mtop-50"><span class="indicators">1</span><span class="ispace"></span><span class="indicators active">2</span><span class="ispace"></span><span class="indicators">3</span></div>'
        ).fadeIn(300);
        refresh();
    });
};

// quiz 2 answer B click
function q2bClick() {
    $('.cleanup').fadeOut(300, function() {
        if ($(window).width() > 550) {
            $('.quiz-banner').css('background-image','url('+background_banner['q2b']+')');
        } else {
            $('.quiz-banner').css('background-image','url('+background_banner['q2b']+')');
            $('.quiz-banner').css('background-position','20% 1%');
        }

        $(this).empty().append(
            '<div class="head2 white">Bliss feels like walking through</div>' +
            '<a style="padding:10px 30px;" class="btn btn-q btn-qa" data-action="q3b">Lush trees and greenery</a>' +
            '<a class="bspace"></a>' +
            '<a style="padding:10px 45px;" class="btn btn-q btn-qb" data-action="q3a">Soft sand and waters</a>' +
            '<div class="mtop-50"><span class="indicators">1</span><span class="ispace"></span><span class="indicators active">2</span><span class="ispace"></span><span class="indicators">3</span></div>'
        ).fadeIn(300);
        refresh();
    });
};

// quiz 3 answer A click
function q3aClick() {
    $('.cleanup').fadeOut(300, function() {
        if ($(window).width() > 550) {
            $('.quiz-banner').css('background-image','url('+background_banner['q3a']+')');
        } else {
            $('.quiz-banner').css('background-image','url('+background_banner['q3a']+')');
            $('.quiz-banner').css('background-position','80% 10%');
        }

        $(this).empty().append(
            '<div class="head2 white">When the sun is out to play</div>' +
            '<a style="padding:10px 40px;" class="btn btn-q btn-qa" data-action="ansA">I\'m in bed hiding away!</a>' +
            '<a class="bspace"></a>' +
            '<a style="padding:10px 30px;" class="btn btn-q btn-qb2" data-action="ansB">Head on out and start the day!<br>*puts on shades*</a>' +
            '<div class="mtop-50"><span class="indicators">1</span><span class="ispace"></span><span class="indicators">2</span><span class="ispace"></span><span class="indicators active">3</span></div>'
        ).fadeIn(300);
        refresh();
    });
};

// quiz 3 answer B click
function q3bClick() {
    $('.cleanup').fadeOut(300, function() {
        if ($(window).width() > 550) {
            $('.quiz-banner').css('background-image','url('+background_banner['q3b']+')');
        } else {
            $('.quiz-banner').css('background-image','url('+background_banner['q3b']+')');
            $('.quiz-banner').css('background-position','65% 85%');
        }

        $(this).empty().append(
            '<div class="head2 white">You love spending time</div>' +
            '<a style="padding:10px 30px;" class="btn btn-q btn-qa" data-action="ansC">With Me, Myself, and I!</a>' +
            '<a class="bspace"></a>' +
            '<a style="padding:10px 60px;" class="btn btn-q btn-qb" data-action="ansD">With my squad!</a>' +
            '<div class="mtop-50"><span class="indicators">1</span><span class="ispace"></span><span class="indicators">2</span><span class="ispace"></span><span class="indicators active">3</span></div>'
        ).fadeIn(300);
        refresh();
    });
};

// quiz result A
function ansAClick() {
    $('.cleanup').fadeOut(300, function() {
        if ($(window).width() > 767) {
            $('.quiz-banner').css('background-image','url('+background_banner['ansA']+')');
        } else {
            $('.quiz-banner').css('background-image','none');
            $('.quiz-wrap').css('height','auto');
            $('.cleanup').css('padding','0');
        }
        $(this).empty().append(

            '<div class="col-md-5 hidden-xs"></div>' +
            '<div class="col-xs-12 nopadding visible-xs"><img src="img/mobile-answer1.jpg" style="width:100%;"></div>' +
            '<div class="col-md-7 col-xs-12 yer">' +
            '<div class="ans-head hidden-xs">You Got:</div>' +
            '<div class="ans-font1 hidden-xs">The Empress</div>' +
            '<div class="ans-desc">UV has nothing on you when you\'re this powerful!Strut your Flawless-ness with the <b>Suncare Hybrid-BB for Sports</b>. Whether on the court or on the streets, you have the world down at your feet! You lead your battles and you leave them on the field, no sweat. Well... More sweat. Wetforce has got you covered while you slay. The more you sweat, the more Wetforce plays!</div>' +
            '<div class="ans-desc mtop-20">Your perfect holiday is in a 5-star getaway, because you demand the best and won\'t settle for less. If it makes you feel like a queen, then that’s where you\'ll want to be seen.</div>' +
            '<div class="ans-divider"></div>' +
            '<div class="ans-desc">Use Shiseido\'s Perfect UV Protector every time you\'re under the sun! Register below for your free sample and tell us how you liked it with the #FreedomFeelsLike hashtag.</div>' +
            '<div class="mtop-30 ans-desc"><b class="sr">SHARE YOUR RESULT</b><span class="bspace2"></span><a class="share-fb fq1"><i class="fa fa-facebook"></i></a><span class="ispace"></span><a href="https://twitter.com/intent/tweet?url=http://shiseido-gsc.webqlo.com&amp;text=You got... The Empress! SHISEIDO SUNCARE, To enjoy a moment that\'s all yours, without reservation. It\'s the ultimate freedom. &amp;hashtags=FreedomFeelsLike" class="share-twitter"><i class="fa fa-twitter"></i></a>' +
            '<span class="ispace"></span><button id="go-to-form" class="quiz-go-form">Get Your 3-Day<br>Suncare Trial Kit</button></div>' +
            '</div>'

        ).fadeIn(300);

        $('#go-to-form').click(function () {
            $('html, body').animate({
                scrollTop: $('#form_section').offset().top
            }, 1000);
        });
        refresh();
    });
};

// quiz result B
function ansBClick() {
    $('.cleanup').fadeOut(300, function() {
        if ($(window).width() > 767) {
            $('.quiz-banner').css('background-image','url('+background_banner['ansB']+')');
        } else {
            $('.quiz-banner').css('background-image','none');
            $('.quiz-wrap').css('height','auto');
            $('.cleanup').css('padding','0');
        }
        $(this).empty().append(

            '<div class="col-md-5 hidden-xs"></div>' +
            '<div class="col-xs-12 nopadding visible-xs"><img src="img/mobile-answer2.jpg" style="width:100%;"></div>' +
            '<div class="col-md-7 col-xs-12 yer">' +
            '<div class="ans-head hidden-xs">You Got:</div>' +
            '<div class="ans-font2 hidden-xs">The Go-getter</div>' +
            '<div class="ans-desc">As long as you\'re out in the sun, you\'re ready to have some fun! Amongst your best friends, the Perfect UV Protector is one to stick by your side until the end! You\'d go through the rain, puddles, or a river just to get stuff done and all these protect you even more from the sun.</div>' +
            '<div class="ans-desc mtop-20">The beach is where you should be. The sun, the sand, anything for the tan. But always remember to only be the sun\'s friend when you\'ve got a bottle of Wetforce in your hand!</div>' +
            '<div class="ans-divider"></div>' +
            '<div class="ans-desc">Use Shiseido\'s Perfect UV Protector every time you\'re under the sun! Register below for your free sample and tell us how you liked it with the #FreedomFeelsLike hashtag.</div>' +
            '<div class="mtop-30 ans-desc"><b class="sr">SHARE YOUR RESULT</b><span class="bspace2"></span><a class="share-fb fq2"><i class="fa fa-facebook"></i></a><span class="ispace"></span><a href="https://twitter.com/intent/tweet?url=http://shiseido-gsc.webqlo.com&amp;text=You got... The Go-getter! SHISEIDO SUNCARE, To enjoy a moment that\'s all yours, without reservation. It\'s the ultimate freedom. &amp;hashtags=FreedomFeelsLike" class="share-twitter"><i class="fa fa-twitter"></i></a>' +
            '<span class="ispace"></span><button id="go-to-form" class="quiz-go-form">Get Your 3-Day<br>Suncare Trial Kit</button></div>' +
            '</div>'

        ).fadeIn(300);

        $('#go-to-form').click(function () {
            $('html, body').animate({
                scrollTop: $('#form_section').offset().top
            }, 1000);
        });

        refresh();
    });
};

// quiz result C
function ansCClick() {
    $('.cleanup').fadeOut(300, function() {
        if ($(window).width() > 767) {
            $('.quiz-banner').css('background-image','url('+background_banner['ansC']+')');
        } else {
            $('.quiz-banner').css('background-image','none');
            $('.quiz-wrap').css('height','auto');
            $('.cleanup').css('padding','0');
        }
        $(this).empty().append(

            '<div class="col-md-5 hidden-xs"></div>' +
            '<div class="col-xs-12 nopadding visible-xs"><img src="img/mobile-answer3.jpg" style="width:100%;"></div>' +
            '<div class="col-md-7 col-xs-12 yer">' +
            '<div class="ans-head hidden-xs">You Got:</div>' +
            '<div class="ans-font3 hidden-xs">The Free Spirit</div>' +
            '<div class="ans-desc">Rain or shine, no matter the weather you\'re ready to head on out! Wherever your flight is bound, you\'re always prepared to hit the ground. The Perfect UV Protector S is gentle on any skin, so you can delight in the moment no matter the situation.</div>' +
            '<div class="ans-desc mtop-20">Live free, throw a dart, and go wherever it lands on the map! You strive to discover what the world has to offer. For every conquest, the Perfect UV Protector S is a must-have for your sensitive skin.</div>' +
            '<div class="ans-divider"></div>' +
            '<div class="ans-desc">Use Shiseido\'s Perfect UV Protector every time you\'re under the sun! Register below for your free sample and tell us how you liked it with the #FreedomFeelsLike hashtag.</div>' +
            '<div class="mtop-30 ans-desc"><b class="sr">SHARE YOUR RESULT</b><span class="bspace2"></span><a class="share-fb fq3"><i class="fa fa-facebook"></i></a><span class="ispace"></span><a href="https://twitter.com/intent/tweet?url=http://shiseido-gsc.webqlo.com&amp;text=You got... The Free Spirit! SHISEIDO SUNCARE, To enjoy a moment that\'s all yours, without reservation. It\'s the ultimate freedom. &amp;hashtags=FreedomFeelsLike" class="share-twitter"><i class="fa fa-twitter"></i></a>' +
            '<span class="ispace"></span><button id="go-to-form" class="quiz-go-form">Get Your 3-Day<br>Suncare Trial Kit</button></div>' +
            '</div>'

        ).fadeIn(300);

        $('#go-to-form').click(function () {
            $('html, body').animate({
                scrollTop: $('#form_section').offset().top
            }, 1000);
        });

        refresh();
    });
};

// quiz result D
function ansDClick() {
    $('.cleanup').fadeOut(300, function() {
        if ($(window).width() > 767) {
            $('.quiz-banner').css('background-image','url('+background_banner['ansD']+')');
        } else {
            $('.quiz-banner').css('background-image','none');
            $('.quiz-wrap').css('height','auto');
            $('.cleanup').css('padding','0');
        }
        $(this).empty().append(

            '<div class="col-md-5 hidden-xs"></div>' +
            '<div class="col-xs-12 nopadding visible-xs"><img src="img/mobile-answer4.jpg" style="width:100%;"></div>' +
            '<div class="col-md-7 col-xs-12 yer">' +
            '<div class="ans-head hidden-xs">You Got:</div>' +
            '<div class="ans-font4 hidden-xs">Be-You-Tiful!</div>' +
            '<div class="ans-desc">You don\'t need the world to tell you what to do. You have no care for haters and UV rays when you\'ve got your Perfect UV Protector HydroFresh. There\'s nothing to fear when you have skin that\'s protected and hydrated!</div>' +
            '<div class="ans-desc mtop-20">Nothing makes you happier than spending quality time to love yourself. And loving yourself means taking care of your skin, which makes the Perfect UV Protector HydroFresh perfect for you!</div>' +
            '<div class="ans-divider"></div>' +
            '<div class="ans-desc">Use Shiseido\'s Perfect UV Protector every time you\'re under the sun! Register below for your free sample and tell us how you liked it with the #FreedomFeelsLike hashtag.</div>' +
            '<div class="mtop-30 ans-desc"><b class="sr">SHARE YOUR RESULT</b><span class="bspace2"></span><a class="share-fb fq4"><i class="fa fa-facebook"></i></a><span class="ispace"></span><a href="https://twitter.com/intent/tweet?url=http://shiseido-gsc.webqlo.com&amp;text=You got... Be-You-Tiful! SHISEIDO SUNCARE, To enjoy a moment that\'s all yours, without reservation. It\'s the ultimate freedom. &amp;hashtags=FreedomFeelsLike" class="share-twitter"><i class="fa fa-twitter"></i></a>' +
            '<span class="ispace"></span><button id="go-to-form" class="quiz-go-form">Get Your 3-Day<br>Suncare Trial Kit</button></div>' +
            '</div>'

        ).fadeIn(300);

        $('#go-to-form').click(function () {
            $('html, body').animate({
                scrollTop: $('#form_section').offset().top
            }, 1000);
        });

        refresh();
    });
};

refresh();

$(".modal-transparent").on('show.bs.modal', function () {
    setTimeout( function() {
        $(".modal-backdrop").addClass("modal-backdrop-transparent");
    }, 0);
});
$(".modal-transparent").on('hidden.bs.modal', function () {
    $(".modal-backdrop").addClass("modal-backdrop-transparent");
});

$(".modal-fullscreen").on('show.bs.modal', function () {
    setTimeout( function() {
        $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
    }, 0);
});
$(".modal-fullscreen").on('hidden.bs.modal', function () {
    $(".modal-backdrop").addClass("modal-backdrop-fullscreen");
});


//$('#playvideo').click(function () {
//     var src = 'http://www.youtube.com/embed/rq1TgCyhHBg?&rel=0&autoplay=0&showinfo=0&wmode=transparent';
//     $('#videoframe').modal('show');
//     $('#videoframe iframe').attr('src', src);
//});
//
//$('.close').click(function () {
//    $('#videoframe iframe').removeAttr('src');
//});

//var player;
//function onYouTubeIframeAPIReady() {
//    player = new YT.Player('video1', { videoId: 'rq1TgCyhHBg' });
//}
//
//$(function () {
//    $('#videos-slide').bind('slide', function () {
//        alert('stop');
//        player.stopVideo();
//    });
//});

if ($(window).width() < 550) {
    $("#sunsafe").attr("src","../img/sun-safe-featured-mobile.jpg");
}


// load  api
var tag = document.createElement('script');
tag.src = "//www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// make player array
var players = new Array();

function onYouTubeIframeAPIReady() {
    players[0] = new YT.Player('player1', {
        playerVars: { modestbranding: 1, rel: 0, controls: 1 },
        videoId: 'poCnlxP2l-k'
    });
    players[1] = new YT.Player('player2', {
        playerVars: { modestbranding: 1, rel: 0, controls: 1 },
        videoId: 'Od64rkAhf3U'
    });
    players[2] = new YT.Player('player3', {
        playerVars: { modestbranding: 1, rel: 0, controls: 1 },
        videoId: 'nqzbWfjqOWA'
    });
    players[3] = new YT.Player('player4', {
        playerVars: { modestbranding: 1, rel: 0, controls: 1 },
        videoId: 'gmZLI3B7PKI'
    });
    //players[3] = new YT.Player('player4', {
    //    playerVars: { modestbranding: 1, rel: 0, controls: 1 },
    //    videoId: 'poCnlxP2l-k'
    //});
}

$('#videos-slide').carousel({interval:false}).bind('slide.bs.carousel', function (e) {
    $(players).each(function(i){
        this.stopVideo();
    });
});

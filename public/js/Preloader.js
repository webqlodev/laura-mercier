Candy.Preloader = function(game){
	// define width and height of the game
	Candy.GAME_WIDTH = 640;
	Candy.GAME_HEIGHT = 960;
};
Candy.Preloader.prototype = {
	preload: function(){
		// set background color and preload image
		this.stage.backgroundColor = '#B4D9E7';
		this.preloadBar = this.add.sprite((Candy.GAME_WIDTH-311)/2, (Candy.GAME_HEIGHT-27)/2, 'preloaderBar');
		this.load.setPreloadSprite(this.preloadBar);
		// load images
		this.load.image('landing-background', 'img/background.png');
		this.load.image('background', 'img/landing-bg.png');
		this.load.image('floor', 'img/floor.png');
		this.load.spritesheet('monster-idle', 'img/test.png');
		//this.load.image('monster-cover', 'img/monster-cover.png');
		this.load.image('monster-cover', 'img/cute-cover.png');
		this.load.image('title', 'img/title2.png');
		this.load.image('game-over', 'img/gameoverlo.png');
		//this.load.image('score-bg', 'img/score-bg.png');
		this.load.image('score-bg', 'img/score-bg2.png');
		this.load.image('button-pause', 'img/button-pause.png');
		this.load.image('star-effect', 'img/stars.png');
		// load spritesheets
		//this.load.spritesheet('candy', 'img/flower.png', 102, 98);
		this.load.spritesheet('candy', 'img/flower.png', 105, 120);
		//this.load.spritesheet('button-start', 'img/button-start.png', 401, 143);
		this.load.spritesheet('button-start', 'img/button-start-new.png', 245, 104);
		this.load.spritesheet('bomb', 'img/bomb-art.png', 100, 102);
		this.load.spritesheet('explosion', 'img/explosion-art.png', 100, 100);
		this.load.spritesheet('life', 'img/life.png', 200, 65.75);
	},
	create: function(){
		// start the MainMenu state
		this.state.start('MainMenu');
	}
};

$(function () {
    $('#product_section .responsive-tabs').responsiveTabs({
        accordionOn: ['xs'] // valid value: xs, sm, md, lg
    });

    $('#product_section').on('click', '.accordion-link', function () {
        $('html, body').animate({
            scrollTop: $(this).offset().top
        }, 0);
    });

    $('.try_it').click(function () {
        $('html, body').animate({
            scrollTop: $('#form').offset().top
        }, 1000);
    });
});

//carousel
(function(){
    $('#ingredients').carousel({ interval: 8000 });
}());

(function(){
    $('.carousel-showmanymoveone .item').each(function(){
        var itemToClone = $(this);

        for (var i=1;i<4;i++) {
            itemToClone = itemToClone.next();

            // wrap around if at end of item collection
            if (!itemToClone.length) {
                itemToClone = $(this).siblings(':first');
            }

            // grab item, clone, add marker class, add to collection
            itemToClone.children(':first-child').clone()
            .addClass("cloneditem-"+(i))
            .appendTo($(this));
        }
    });
}());

//go section
$(document).on('click', '.menu', function(){
    var the_h = $(this).attr('atr');
    var top = $('#'+the_h).offset().top;

    $('html, body').animate({
        scrollTop: top
    }, 1000);
});

//twitter pop
(function($){
  $.fn.customerPopup = function (e, intWidth, intHeight, blnResize) {
    // Prevent default anchor event
    e.preventDefault();

    // Set values for window
    intWidth = intWidth || '600';
    intHeight = intHeight || '650';
    strResize = (blnResize ? 'yes' : 'no');

    // Set title and open popup with focus on it
    var strTitle = ((typeof this.attr('title') !== 'undefined') ? this.attr('title') : 'Social Share'),
        strParam = 'width=' + intWidth + ',height=' + intHeight + ',resizable=' + strResize,
        objWindow = window.open(this.attr('href'), strTitle, strParam).focus();
  }

    $(document).ready(function ($) {
        $('.customer.share').on("click", function(e) {
            $(this).customerPopup(e);
        });
    });

}(jQuery));

Candy.MainMenu = function(game){};
Candy.MainMenu.prototype = {
	create: function(){
		// display images
		this.add.sprite(0, 0, 'background');
		this.add.sprite((Candy.GAME_WIDTH-540)/2, 95, 'title');
		// add the button that will start the game
		this.add.button(Candy.GAME_WIDTH-430-10, Candy.GAME_HEIGHT-250-10, 'button-start', this.startGame, this, 1, 0, 3);
	},
	startGame: function() {
		// start the Game state
		this.state.start('Game');
	}
};

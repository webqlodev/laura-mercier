<div class="panel-heading text-center">
    <div class="gotham-bold-24pt text-spacing">Redeem Your Deluxe <br>Sample Now</div>
    <div class="gotham-book-16pt pt-3">Kindly fill in the required <br>form below.</div>
</div>
<div class="panel-body gotham-book-16pt text-center">
    <form method="post" action="/" data-toggle="validator" class="">
        <div class="form-group {{ $errors->has('fullname')? 'has-error' : '' }}">
            <div class="col-md-2 col-xs-12 label-required upperfont" for="fullname">Name:</div>
            <div class="col-md-10 col-xs-12 "><input id="fullname" class="form-input" type="text" name="fullname" value="{{ old('fullname') }}" placeholder="Please enter your name" required/></div>
            <div class="help-block with-errors">{{ $errors->has('fullname')? $errors->first('fullname') : '' }}</div>
        </div>

        <div class="form-group {{ $errors->has('email')? 'has-error' : '' }}">
            <div class="col-md-2 col-xs-12 label-required upperfont" for="email">Email:</div>
            <div class="col-md-10 col-xs-12"><input id="email" class="form-input" type="email" name="email" value="{{ old('email') }}" placeholder="Please enter your email" required/></div>
            <div class="help-block with-errors">{{ $errors->has('email')? $errors->first('email') : '' }}</div>
        </div>

        <div class="form-group {{ $errors->has('location')? 'has-error' : '' }}">
            <div class="col-xs-12 label-required" for="location">I would like to collect my <br>Deluxe sample at</div>
            <div class="col-xs-12 wrap"><select id="location" class="form-select" name="location" required>
                <option value="">Please choose one</option>
                <option>PAVILION KUALA LUMPUR</option>
                <option>1UTAMA SHOPPING CENTRE</option>
                <option>SUNWAY PYRAMID SHOPPING MALL</option>
                <option>GURNEY PLAZA, PENANG</option>
                <option>AEON TEBRAU CITY, JOHOR BAHRU</option>
            </select> 
            </div>
            <div class="help-block with-errors">{{ $errors->has('location')? $errors->first('location') : '' }}</div>
        </div>

        <div class="form-group">
            <div class="col-xs-12 gotham-book-13pt text-left text-justify">
                <b>By clicking “Submit”, I agree to receive occasional email updates on Laura Mercier-related promotions and activities.</b>         
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-12">
                <div class="checkbox gotham-book-13pt text-left text-justify">
                    <label><b><input type="checkbox" class="awesome" required />I have read and agreed to the <a data-toggle="modal" data-target="#privacy_policy_new">Privacy Policy</a> and <a data-toggle="modal" data-target="#tnc">Terms and Conditions</a></b></label>
                </div>
                <div class="help-block with-errors"></div>
            </div>
        </div>

        <div class="form-group text-center">
            <div class="col-xs-12">
            {{ csrf_field() }}
            <button type="submit" class="gotham-bold-13pt link-button">Submit To Redeem</button>
            </div>
        </div> 

        <div class="form-group gotham-book-9pt text-center">
            <div class="col-xs-12">
            *Limited samples available. Redemption is only applicable at selected Laura Mercier stores. Terms and conditions apply.
            </div>
        </div>
    </form>
</div>

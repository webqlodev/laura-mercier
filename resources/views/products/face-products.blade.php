<div id="face" class="container">
	<div class="row">
		<div id="face_decs" class="carousel slide" data-ride="carousel" data-interval="false">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
		    <li data-target="#face_decs" data-slide-to="0" class="active"></li>
		    <li data-target="#face_decs" data-slide-to="1"></li>
		    <li data-target="#face_decs" data-slide-to="2"></li>
		    <li data-target="#face_decs" data-slide-to="3"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">
		    <div class="item active">
		   		<div class="product-bg-img fix-height">
		   			<div class="products_header gotham-bold-32pt text-spacing">
		   				holiday <br>flawless face
		   			</div>
		   			<div class="swipe gotham-book-13pt">
	   					Swipe to find out more
		   			</div>
		   			<div class="arrow-swipe">
		   				<i class="far fa-long-arrow-alt-right"></i>
		   			</div>
		   		</div>
		    </div>

		     <div class="item">
		    	<div class="fix-height">
		    		<div class="products-content">
		    			<div class="col-md-12"><img class="img-fluid" src="{{asset('../img/face/primeset.jpg')}}"></div>
		    			<div class="col-md-12 gotham-bold-16pt upperfont">
		    				prime, set & glow flawless face collection
		    				<br>rm219
		    			</div>
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				<i>
		    					Set Includes :<br>
		    					Translucent Loose Setting Powder 9.3g, Foundation Primer - Radiance 15ml & Matte Makeup Radiance Baked Powder 1.80 g
		    				</i>
		    			</div>--}}
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br><br>
		    				Laura Mercier icons in portable sizes. This bestselling translucent powder sets make up for longer wear blurring lines and imperfections. Primer creates a smooth flawless application. Its pearl tint turns up radiance and tones down the look of pores. An ultra-smooth baked face powder provides all day soft radiance. Comes with a mini fan brush for a flawless application.	
		    			</div>--}}
		    			<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				A curated set of Laura's iconic products that creates the perfect Flawless Face look with a soft radiant glow.
		    			</div>
		    		</div>
		    	</div>
		    </div>

		    <div class="item">
		    	<div class="fix-height">
		    		<div class="products-content">
		    			<div class="col-md-12"><img class="img-fluid" src="{{asset('../img/face/pretapowder.jpg')}}"></div>
		    			<div class="col-md-12 gotham-bold-16pt upperfont">
		    				pret-a-powder limited edition powder & puff
		    				<br>rm209
		    			</div>
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				<i>
		    					Set Includes :<br>
		    					Translucent Loose Setting Powder 29g
		    				</i>
		    			</div>--}}
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br><br>
		    				Housed in a limited edition holiday theme inspired special decoration jar, this multiple awards winner is a cult favourite global bestseller. Powder feels incredibly smooth and silly-light, blends effortlessly without adding weight or texture keeping your makeup pristine. Also available in Translucent Medium Deep colour.
		    			</div>--}}
		    			<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				Limited Holiday edition of the #1 setting powder complete with a Velour Puff for seamless application. Available in <strong>Translucent</strong> and <strong>Translucent Medium Deep</strong>.
		    			</div>
		    		</div>
		    	</div>
		    </div>

		    <div class="item">
		    	<div class="fix-height">
		    		<div class="products-content">
		    			<div class="col-md-12"><img class="img-fluid" src="{{asset('../img/face/goset.jpg')}}"></div>
		    			<div class="col-md-12 gotham-bold-16pt upperfont">
		    				go, set & glow trio setting duo 
		    				<br>rm199
		    			</div>
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				<i>
		    					Set Includes :<br>
		    					Translucent Loose Setting Powder 9.3g & NEW Translucent Loose Setting Powder - Glow 9.3g
		    				</i>
		    			</div>--}}
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br><br>
		    				Regardless of matte or glow finish or both for added dimension, these translucent powders offer soft-focus effects that won't settle into lines or pores. Natural looking, never powdery nor cakey, you can party all night looking flawless knowing your makeup is intact.
		    			</div>--}}
		    			<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				Iconic Tranlucent Loose Setting Powders - matte and glow finished, set makeup for 12 hours without adding weight or texture.
		    			</div>
		    		</div>
		    	</div>
		    </div>

		  </div>
		  <a class="left carousel-control" href="#face_decs" role="button" data-slide="prev">
		  	{{--<span class="glyphicon glyphicon-chevron-left"></span>--}}
    		{{--<span class="sr-only">Previous</span>--}}
		  </a>
		  <a class="right carousel-control" href="#face_decs" role="button" data-slide="next">
		  	{{--<span class="glyphicon glyphicon-chevron-right"></span>--}}
    		{{--<span class="sr-only">Next</span>--}}
		  </a>
		</div>
	</div>
</div>

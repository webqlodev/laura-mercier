<div id="eyelips" class="container">
	<div class="row">
		<div id="eyelips_decs" class="carousel slide" data-ride="carousel" data-interval="false">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
		    <li data-target="#eyelips_decs" data-slide-to="0" class="active"></li>
		    <li data-target="#eyelips_decs" data-slide-to="1"></li>
		    <li data-target="#eyelips_decs" data-slide-to="2"></li>
		    <li data-target="#eyelips_decs" data-slide-to="3"></li>
		    <li data-target="#eyelips_decs" data-slide-to="4"></li>
		    <li data-target="#eyelips_decs" data-slide-to="5"></li>
		    <li data-target="#eyelips_decs" data-slide-to="6"></li>
		    <li data-target="#eyelips_decs" data-slide-to="7"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">
		    <div class="item active">
		   		<div class="product-bg-img fix-height">
		   			<div class="products_header gotham-bold-32pt text-spacing">
		   				holiday <br>eyes & lips
		   			</div>
		   			<div class="swipe gotham-book-13pt">
	   					Swipe to find out more
		   			</div>
		   			<div class="arrow-swipe">
		   				<i class="far fa-long-arrow-alt-right"></i>
		   			</div>
		   		</div>
		    </div>

		      <div class="item">
		    	<div class="fix-height">
		    		<div class="products-content">
		    			<div class="col-md-12"><img class="img-fluid" src="{{asset('../img/palettes/masterclass.jpg')}}"></div>
		    			<div class="col-md-12 gotham-bold-16pt upperfont">
		    				master class artistry in light holiday illuminations edition 
		    				<br>rm495
		    			</div>
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				<i>
		    					Set Includes :<br>
		    					Translucent Loose Setting Powder 9.3g, Foundation Primer - Radiance 15ml & Matte Makeup Radiance Baked Powder 1.80 g
		    				</i>
		    			</div>--}}
		    			<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				A limited edition collection with iconic shades, finishes and liners to create effortless looks.
		    			</div>
		    		</div>
		    	</div>
		    </div>

		    <div class="item">
		    	<div class="fix-height">
		    		<div class="products-content">
		    			<div class="col-md-12"><img class="img-fluid" src="{{asset('../img/palettes/nightsout.jpg')}}"></div>
		    			<div class="col-md-12 gotham-bold-16pt upperfont">
		    				nights out eye shadow palette 
		    				<br>rm260
		    			</div>
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				<i>
		    					Set Includes :<br>
		    					Translucent Loose Setting Powder 9.3g & NEW Translucent Loose Setting Powder - Glow 9.3g
		    				</i>
		    			</div>--}}
		    			<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				An eyeshadow palette featuring 12 dazzling, highly pigmented shades that span from light and luminous to deeply dramatic.
		    			</div>
		    		</div>
		    	</div>
		    </div>

		  	 <div class="item">
		    	<div class="fix-height">
		    		<div class="products-content">
		    			<div class="col-md-12"><img class="img-fluid" src="{{asset('../img/eyesNlips/shadow&lights.jpg')}}"></div>
		    			<div class="col-md-12 gotham-bold-16pt upperfont">
		    				shadows & lights mini caviar stick collection 
		    				<br>rm170
		    			</div>
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				<i>
		    					Set Includes :<br>
		    					4 x Caviar Stick Eye Colour in Rosegold, Amethyst, Moonlight & Au Natural 1gm each
		    				</i>
		    			</div>--}}
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br><br>
		    				A multitasking and long lasting creamy eye shadow that can line, highlight and create smoky looks. Transfer and crease-resistant formula, this palette offers 2 matte and shimmer finish to deliver endless options for effortlessly beautiful eyes that last from day to night.
		    			</div>--}}
		    			<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				Shadow, highlight, line and define. Matte and metallic shades glide on smoothly and blend effortlessly for the perfect sultry, smoky eye.
		    			</div>
		    		</div>
		    	</div>
		    </div>


		    <div class="item">
		    	<div class="fix-height">
		    		<div class="products-content">
		    			<div class="col-md-12"><img class="img-fluid" src="{{asset('../img/eyesNlips/shadow&brights.jpg')}}"></div>
		    			<div class="col-md-12 gotham-bold-16pt upperfont">
		    				shadows & brights metallic caviar stick eye colour collection 
		    				<br>rm170
		    			</div>
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				<i>
		    					Set Includes :<br>
		    					4 x Caviar Stick Eye Colour in Intense Amethyst, Intense Rose Gold, Intense Moonlight & Intense Copper 1gm each
		    				</i>
		    			</div>--}}
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br><br>
		    				A limited edition collection of long-lasting, metallic creamy eye shadow sticks that glide seamlessly onto lids, blends and layers beautifully with a rich pigment-payoff, creating the perfect metallic sultry, smoky eye.
		    			</div>--}}
		    			<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				A limited edition collection of long-lasting, metallic creamy eye shadow sticks that can also line, highlight, and create smoky looks.
		    			</div>
		    		</div>
		    	</div>
		    </div>

		     <div class="item">
		    	<div class="fix-height">
		    		<div class="products-content">
		    			<div class="col-md-12"><img class="img-fluid" src="{{asset('../img/eyesNlips/eyelights.jpg')}}"></div>
		    			<div class="col-md-12 gotham-bold-16pt upperfont">
		    				eye lights deluxe mini eye collection 
		    				<br>rm140
		    			</div>
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				<i>
		    					Set Includes :<br>
		    					2 x Caviar Stick Eye Colour 1g each & Full Blown Volume Supreme Lash Building Mascara 5.7g
		    				</i>
		    			</div>--}}
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br><br>
		    				The multitasking eye shadow sticks deliver precise high-definition application with long wearing rich pigment-payoff. Transfer and crease-resistant, colour glides on smoothly. The mascara gives lashes a thicker, longer, curled appearance with each stroke.
		    			</div>--}}
		    			<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				Light up your eyes with this exclusive, limited-edition trio of magnetic eye favorites.
		    			</div>
		    		</div>
		    	</div>
		    </div>

		    <div class="item">
		    	<div class="fix-height">
		    		<div class="products-content">
		    			<div class="col-md-12"><img class="img-fluid" src="{{asset('../img/eyesNlips/starlightmini.jpg')}}"></div>
		    			<div class="col-md-12 gotham-bold-16pt upperfont">
						Starlights Mini Lip Glacé Collection
		    				<br>rm165
		    			</div>
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				<i>
		    					Set Includes :<br>
		    					5 x Lip Glace in Cosmic, Bare Pink, Pink Pop, Rose Gold Accent & BonBon 2.8gm each
		    				</i>
		    			</div>--}}
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br><br>
		    				A high shine, perfectly pigmented lip gloss with rich long-lasting colour creates the appearance of fuller lips. Infused with a unique blend of anti-aging moisturisers and anti-oxidants, this non-sticky formula glides on smoothly without feathering or bleeding.
		    			</div>--}}
		    			<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				Five festive, perfectly pigmented lip glosses with high-shine and sheer coverage.
		    			</div>
		    		</div>
		    	</div>
		    </div>

		    <div class="item">
		    	<div class="fix-height">
		    		<div class="products-content">
		    			<div class="col-md-12"><img class="img-fluid" src="{{asset('../img/eyesNlips/liplights.jpg')}}"></div>
		    			<div class="col-md-12 gotham-bold-16pt upperfont">
		    				lip lights deluxe mini lip collection 
		    				<br>rm140
		    			</div>
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				<i>
		    					Set Includes :<br>
		    					Velour Lovers 2.75g, Lip Glace 2.8g & Lip Pencil 1.49g
		    				</i>
		    			</div>--}}
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br><br>
		    				Everything you need to achieve a sexy pout. Begin with the perfectly textured lip pencil to outline your desired lips shape. Lip pencil aids to prolong wear of all lip colours. Apply Velour Lovers a satin-matte creamy long wear formula blending towards the lined lips. Finish with a dab of Lip Glace on the centre of both upper and lower lips.
		    			</div>--}}
		    			<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				A limited edition lip collection featuring our cult favorites Lip Glace, Lip Colour, and Lip Pencil.
		    			</div>
		    		</div>
		    	</div>
		    </div>

		  </div>
		  <a class="left carousel-control" href="#eyelips_decs" role="button" data-slide="prev">
		  	{{--<span class="glyphicon glyphicon-chevron-left"></span>--}}
    		{{--<span class="sr-only">Previous</span>--}}
		  </a>
		  <a class="right carousel-control" href="#eyelips_decs" role="button" data-slide="next">
		  	{{--<span class="glyphicon glyphicon-chevron-right"></span>--}}
    		{{--<span class="sr-only">Next</span>--}}
		  </a>
		</div>
	</div>
</div>

<div id="palettes" class="container">
	<div class="row">
		<div id="palettes_decs" class="carousel slide" data-ride="carousel" data-interval="false">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
		    <li data-target="#palettes_decs" data-slide-to="0" class="active"></li>
		    <li data-target="#palettes_decs" data-slide-to="1"></li>
		    <li data-target="#palettes_decs" data-slide-to="2"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">
		    <div class="item active">
		   		<div class="product-bg-img fix-height">
		   			<div class="products_header gotham-bold-32pt text-spacing">
		   				holiday glow
		   			</div>
		   			<div class="swipe gotham-book-13pt">
	   					Swipe to find out more
		   			</div>
		   			<div class="arrow-swipe">
		   				<i class="far fa-long-arrow-alt-right"></i>
		   			</div>
		   		</div>
		    </div>

		    <div class="item">
		    	<div class="fix-height">
		    		<div class="products-content">
		    			<div class="col-md-12"><img class="img-fluid" src="{{asset('../img/palettes/stardust.jpg')}}"></div>
		    			<div class="col-md-12 gotham-bold-16pt upperfont">
		    				stardust radiant glow palette
		    				<br>rm209
		    			</div>
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				<i>
		    					Set Includes :<br>
		    					Translucent Loose Setting Powder 29g
		    				</i>
		    			</div>--}}
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br><br>
		    				A face illuminator with an innovative flat baked powder formula that provides an even pick-up of light-reflecting pigments for a multi-dimensional finish designed to complement all skin tones.
		    			</div>--}}
		    			<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				A limited edition face palette featuring three luminous, versatile highlighting powders for a customizable radiance.
		    			</div>
		    		</div>
		    	</div>
		    </div>

		    <div class="item">
		    	<div class="fix-height">
		    		<div class="products-content">
		    			<div class="col-md-12"><img class="img-fluid" src="{{asset('../img/palettes/magichour.jpg')}}"></div>
		    			<div class="col-md-12 gotham-bold-16pt upperfont">
		    				magic hour face illuminator palette 
		    				<br>rm245
		    			</div>
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				<i>
		    					Set Includes :<br>
		    					Translucent Loose Setting Powder 9.3g, Foundation Primer - Radiance 15ml & Matte Makeup Radiance Baked Powder 1.80 g
		    				</i>
		    			</div>--}}
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br><br>
		    				An illuminating powder that highlight skin with pure light-reflecting pigments for a colour-true, multi-dimensional and luminous finish that complements all skin tones. Not just to be used on face and eyes, a light sweep on the décolletage gives an all over radiance.
		    			</div>--}}
		    			<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				A limited edition collection of long-wearing, buildable and versatile highlighting powders that provide a natural-looking, radiant, allover glow.
		    			</div>
		    		</div>
		    	</div>
		    </div>

		  </div>
		  <a class="left carousel-control" href="#palettes_decs" role="button" data-slide="prev">
		  	{{--<span class="glyphicon glyphicon-chevron-left"></span>--}}
    		{{--<span class="sr-only">Previous</span>--}}
		  </a>
		  <a class="right carousel-control" href="#palettes_decs" role="button" data-slide="next">
		  	{{--<span class="glyphicon glyphicon-chevron-right"></span>--}}
    		{{--<span class="sr-only">Next</span>--}}
		  </a>
		</div>
	</div>
</div>

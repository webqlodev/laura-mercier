<div id="skincare" class="container">
	<div class="row">
		<div id="skincare_decs" class="carousel slide" data-ride="carousel" data-interval="false">
		  <!-- Indicators -->
		  <ol class="carousel-indicators">
		    <li data-target="#skincare_decs" data-slide-to="0" class="active"></li>
		    <li data-target="#skincare_decs" data-slide-to="1"></li>
		    <li data-target="#skincare_decs" data-slide-to="2"></li>
		    <li data-target="#skincare_decs" data-slide-to="3"></li>
		    <li data-target="#skincare_decs" data-slide-to="4"></li>
		    <li data-target="#skincare_decs" data-slide-to="5"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">
		    <div class="item active">
		   		<div class="product-bg-img fix-height">
		   			<div class="products_header gotham-bold-32pt text-spacing">
		   				holiday <br>body & bath
		   			</div>
		   			<div class="swipe gotham-book-13pt">
	   					Swipe to find out more
		   			</div>
		   			<div class="arrow-swipe">
		   				<i class="far fa-long-arrow-alt-right"></i>
		   			</div>
		   		</div>
		    </div>

		    <div class="item">
		    	<div class="fix-height">
		    		<div class="products-content">
		    			<div class="col-md-12"><img class="img-fluid" src="{{asset('../img/skincare/ambrevanille.jpg')}}"></div>
		    			<div class="col-md-12 gotham-bold-16pt upperfont">
						Luxe Indulgences Ambre Vanillé Luxe Body Collection 
		    				<br>rm359
		    			</div>
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				<i>
		    					Set Includes :<br>
		    					Crème Body Wash 100ml, Soufflè Body Crème 200ml, Eau De Toilette 15ml, Honey Bath 200ml & Wooden Honey Dipper
		    				</i>
		    			</div>--}}
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br><br>
		    				Ambre Vanillè blends the soft sensuality of Amber with the warm comfort of Vanilla, two of Laura's favourite notes, creating a fragrance of feminine elegance and sophistication. The Honey Bath shower crème leaves skin supple and senses relaxed while Crème Body Wash creates a creamy lather that cleanses skin leaving it feeling smooth and moisturized. Complete with Soufflè Body Crème and Eau de Toilette for a lasting lingering scent.
		    			</div>--}}
		    			<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				A limited edition collection featuring an assortment of Ambre Vanillé Body & Bath favorites.
		    			</div>
		    		</div>
		    	</div>
		    </div>

		    <div class="item">
		    	<div class="fix-height">
		    		<div class="products-content">
		    			<div class="col-md-12"><img class="img-fluid" src="{{asset('../img/skincare/almondcoconutmilk.jpg')}}"></div>
		    			<div class="col-md-12 gotham-bold-16pt upperfont">
		    				luxe indulgences almond coconut milk luxe body collection 
		    				<br>rm359
		    			</div>
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				<i>
		    					Set Includes :<br>
		    					Crème Body Wash 100ml, Soufflè Body Crème 200ml, Eau De Toilette 15ml, Honey Bath 200ml & Wooden Honey Dipper
		    				</i>
		    			</div>--}}
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br><br>
		    				Inspired by the aroma of Almond Milk and Coconut Milk, Laura Mercier created this seductive, warm and comforting blend of gourmande ingredients. Her Almond Coconut Milk creation includes succulent notes of Coconut, Almond and Vanilla, combined with Heliotrope and Musk for a rich, sensual and alluring experience.
		    			</div>--}}
		    			<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				A limited edition collection featuring an assortment of Almond Coconut Milk Body & Bath favorites.
		    			</div>
		    		</div>
		    	</div>
		    </div>

		    <div class="item">
		    	<div class="fix-height">
		    		<div class="products-content">
		    			<div class="col-md-12"><img class="img-fluid" src="{{asset('../img/skincare/soufflecreme.jpg')}}"></div>
		    			<div class="col-md-12 gotham-bold-16pt upperfont">
						Luxe Indulgences Soufflé Body Crème Collection
		    				<br>rm209
		    			</div>
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				<i>
		    					Set Includes :<br>
		    					4 x Mini Soufflè Body Crème 60ml each
		    				</i>
		    			</div>--}}
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br><br>
		    				A silky, smooth crème that luxuriously and instantly enriches skin with its abundant skin nourishing ingredients. For skin that deserves the ultimate in luxury, it lingers behind a unique scent upon application. This limited edition Soufflè Body Creme consists of Almond Coconut Milk, Ambre Vanille, Crème de Pistache and Fresh Fig.
		    			</div>--}}
		    			<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				A unique, limited-edition collection features a curated array of Laura’s most iconic Soufflé Body Crèmes in chic mini sizes ideal for travel.
		    			</div>
		    		</div>
		    	</div>
		    </div>

		    <div class="item">
		    	<div class="fix-height">
		    		<div class="products-content">
		    			<div class="col-md-12"><img class="img-fluid" src="{{asset('../img/skincare/handnbody.jpg')}}"></div>
		    			<div class="col-md-12 gotham-bold-16pt upperfont">
		    				luxe indulgences hand & body crèmes collection 
		    				<br>rm159
		    			</div>
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				<i>
		    					Set Includes :<br>
		    					4 x Hand & Body Crème 30ml each
		    				</i>
		    			</div>--}}
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br><br>
		    				A nourishing hand crèmes that leaves your hands velvety soft with the unique essence of Ambre Vanille, Almond Coconut Milk, Crème Brulee and Tarte Au Citron. Deeply moisturizing, the creme continues to nourish even after it is absorbed into the skin.
		    			</div>--}}
		    			<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				Limited-edition collection features a curated array of four Mini Hand and Body Crèmes.
		    			</div>
		    		</div>
		    	</div>
		    </div>

		    <div class="item">
		    	<div class="fix-height">
		    		<div class="products-content">
		    			<div class="col-md-12"><img class="img-fluid" src="{{asset('../img/skincare/infusionderose.jpg')}}"></div>
		    			<div class="col-md-12 gotham-bold-16pt upperfont">
		    				infusion de rose nourishing collection
		    				<br>rm495
		    			</div>
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				<i>
		    					Set Includes :<br>
		    					Infusion de Rose Nourishing Oil, Infusion de Rose Nourishing Crème,Face Polish
		    				</i>
		    			</div>--}}
		    			{{--<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br><br>
		    				Get glowing with Infusion de Rose Nourishing Collection. Infusion de Rose Nourishing Oil leaves skin a healthy glow-soft, radiant and renewed. Infusion de Rose Nourishing Crème provides supreme moisturization immediately and all day. Infusion de Rose Lip Balm locks in moisture to smooth and comfort lips.
		    			</div>--}}
		    			<div class="col-md-12 gotham-book-13pt line-spacing text-center text-justify">
		    				<br>
		    				 A limited edition trio of skincare favorites to soften, visibly smooth and balance skin, featuring Infusion de Rose Nourishing Crème. Infusion de Rose Nourishing Oil and Infusion de Rose Nourishing Lip Balm.
		    			</div>
		    		</div>
		    	</div>
		    </div>

		  </div>
		  	<a class="left carousel-control" href="#skincare_decs" role="button" data-slide="prev">
		  		{{--<span class="glyphicon glyphicon-chevron-left"></span>--}}
    			{{--<span class="sr-only">Previous</span>--}}
		  	</a>
		    <a class="right carousel-control" href="#skincare_decs" role="button" data-slide="next">
		    	{{--<span class="glyphicon glyphicon-chevron-right"></span>--}}
    			{{--<span class="sr-only">Next</span>--}}
		    </a>
		</div>
	</div>
</div>

<div id="palettes-desk" class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <button class="close" type="button" data-dismiss="modal">×</button>
        <div id="palettesmodalcarousel" class="carousel">
        <div class="carousel-inner">

           <div class="item active">
            <div class="col-xs-12">
              <div class="row">
                <div class="col-xs-5 text-center pt-2">
                  <img class="img-desk" src="{{asset('/img/palettes_desk/stardust.jpg')}}"/>
                </div>
                <div class="col-xs-7">
                  <div class="row">
                    <div class="col-md-12 gotham-bold-16pt upperfont">
                      stardust radiant glow palette
                      <br>rm209
                    </div>
                    <div class="col-md-12 gotham-book-13pt">
                      <br>
                      A limited edition face palette featuring three luminous, versatile highlighting powders for a customizable radiance.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="item">
            <div class="col-xs-12">
              <div class="row">
                <div class="col-xs-5 text-center pt-2">
                  <img class="img-desk" src="{{asset('/img/palettes_desk/magichour.jpg')}}"/>
                </div>
                <div class="col-xs-7">
                  <div class="row">
                    <div class="col-md-12 gotham-bold-16pt upperfont">
                      magic hour face illuminator palette 
                      <br>rm245
                    </div>
                    <div class="col-md-12 gotham-book-13pt">
                      <br>
                      A limited edition collection of long-wearing, buildable and versatile highlighting powders that provide a natural-looking, radiant, allover glow.
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div><!-- /.carousel-inner -->
        <a class="left carousel-control" href="#palettesmodalcarousel" role="button" data-slide="prev">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#palettesmodalcarousel" role="button" data-slide="next">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
      </div>
      </div><!-- /.modal-body -->
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
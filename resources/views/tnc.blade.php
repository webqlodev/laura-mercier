<style>
	.sheading{text-decoration:underline;}
    .modal-body {padding:30px!important;}
</style>
<div class="modal fade" id="tnc" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content" style="color:#555; border-radius:0;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" style="text-align: center; font-weight:bold;">Terms & Conditions</h4>
		    </div>
			<div class="modal-body">
				<ol>
					<li>This Campaign is organized by Laura Mercier Malaysia.</li>
					<li>The exclusive Deluxe Sample are while stocks last only. One redemption per customer please. Duplicate redemptions will not be entertained.</li>
					{{-- <li>The Campaign will run on Facebook from 27 July 2017 until 31 August 2017.</li> --}}
					<li>The Campaign is open to residents of Malaysia and permanent residents aged 18 years old and above with valid NRIC. By entering the campaign you warrant that you meet these requirements.</li>
					<li>The following group of persons shall not be eligible to participate in the campaign: Employees of the Organizer including its affiliated and related companies and their immediate family members (children, parents, brothers and sisters, including spouses); and/or representatives, employees, servants and/or agents of advertising and/or promotion service providers of Organizer including its affiliated and related companies, and their immediate family members (children, parents, brothers and sisters including spouses).</li>
					<li>The Organizer reserves the right to suspend, modify, terminate or cancel the campaign at any time. These conditions may be amended from time to time by the Organizer without prior notice. All entries received outside the campaign period shall automatically be disqualified.</li>
					<li>The Organizer reserves the right at its absolute discretion to substitute any of the sample at any time without prior notice. All samples are given on an "as is basis.</li>
					<li>Failure by the Organizer to enforce any of its rights at any time does not constitute a waiver of those rights.</li>
					<li>This Campaign is in no way sponsored, endorsed or administered by, or associated with Facebook. You are providing your information to Laura Mercier Malaysia and not to Facebook. The information you provide will only be used for the fulfillment of this campaign only.</li>
				</ol>
			</div><!-- modal body -->
		</div>
	</div>
</div>
	

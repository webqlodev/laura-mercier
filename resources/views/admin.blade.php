@extends('layouts.app')

@section('css')
    <link href="{{ asset('css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/buttons.dataTables.min.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header" style="padding:10px 0;">
                <img width="300" src="{{ asset('img/LauraMercier_logo.png') }}" />
            </div>

            <ul class="nav navbar-nav navbar-right" style="position:relative;bottom:-5px;">
                <li><a href="{{ route('logout') }}">Logout</a></li>
            </ul>
        </div>
    </nav>
    <div class="container">
        <div class="row">        
            @if ( Auth::user()->name == 'webqlo' )
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-10">
                        <blockquote>
                            <strong>Start Time: </strong><span id="start-time">{{ Carbon\Carbon::parse( $campaign_time['start'] )->format('j F Y, H:i') }}</span>
                            <br>
                            <strong>End Time: </strong><span id="end-time">{{ Carbon\Carbon::parse( $campaign_time['end'] )->format('j F Y, H:i') }}</span>
                        </blockquote>
                    </div>
                    <button type="button" class="btn btn-primary col-xs-2" data-toggle="modal" data-target="#campaign_date_modal">
                        Edit
                    </button>
                </div>
            </div>
            <div class="col-xs-12">
                <a class="btn btn-primary" href="/generate-code/5000" target="_blank" style="margin-top: 10px;">Generate 5k Unique Codes</a>
                <a class="btn btn-primary" href="/export-code-list" target="_blank" style="margin-top: 10px;">Export Generated code XLSX</a>
                <br/>
            </div>
            <div class="col-xs-12"><br/></div>
            @endif
            <div class="col-xs-12 col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Registration Summary
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <label>Pick Register Date</label>

                            <input type="text" id="registration-datepicker" class="form-control readonly-white-bg" readonly />
                        </div>

                        <table id="registration-table" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Register Date</th>
                                    <th>Total Register</th>
                                </tr>
                            </thead>

                            <tbody>
                                <!-- table body -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if ( Auth::user()->name == 'webqlo' )
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Social Media Click Actions
                    </div>

                    <div class="table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Facebook</th>
                                    <th>{{ $social['facebook'] }}</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        Register
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <a href="{{ route('export') }}" target="_blank" id="export-link" class="btn btn-default pull-right">Download <span id="export-text">All Records</span> in XLS</a>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <div class="form-group">
                                    <label>Pick Register Date</label>

                                    <input type="text" id="customer-datepicker" class="form-control readonly-white-bg" readonly />
                                </div>
                            </div>
                        </div>

                        <table id="customer-table" class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Outlet</th>
                                    <th>Redeem Code</th>
                                    <th>Register Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                <!-- table body -->
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="campaign_date_modal" tabindex="-1" role="dialog" aria-labelledby="modal_title" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_title">Edit Campaign Date</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Please select the start and end date of the campaign.</p>
                <p>
                    Example:
                    <br>
                    Start date is 1 April 2018 00:00:00am, End date is 30 April 2018 at 11:59:59pm.
                    <br>
                    Just choose <b>1 April 2018</b> and <b>30 April 2018</b> will do.
                </p>
                <div class="input-daterange input-group" id="datepicker">
                    <input id="start_date" type="text" class="input-sm form-control" name="start" />
                    <span class="input-group-addon">to</span>
                    <input id="end_date" type="text" class="input-sm form-control" name="end" />
                </div>
                <button id="save_btn" type="submit" class="btn btn-primary">Save changes</button>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('js/responsive.bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/dataTables.buttons.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
          var reservationTable = $('#registration-table').DataTable({
                processing: true,
                serverSide: true,
                dom: 'lrtip',
                responsive: true,
                ajax: {
                    url: '{{ route('registration') }}',
                    type: 'POST',
                    data: function (data) {
                        data.date = $('#registration-datepicker').val();
                    }
                },
                columns: [
                    {
                        data: 'date',
                        render: function (data, type, row, meta) {
                            return moment(data).format('D MMM YYYY (ddd)');
                        }
                    },
                    {
                        data: 'registration_count'
                    },
                ]
            });

            var customerTable = $('#customer-table').DataTable({
                processing: true,
                serverSide: true,
                dom: 'lfrtip',
                responsive: true,
                ajax: {
                    url: '{{ route('customers') }}',
                    type: 'POST',
                    data: function (data) {
                        data.date = $('#customer-datepicker').val();
                    }
                },
                columns: [
                    {
                        data: 'fullname'
                    },
                    {
                        data: 'email'
                    },
                    {
                        data: 'redeem_location'
                    },
                    {
                        data: 'verification_code'
                    },
                    {
                        data: 'created_at',
                        searchable: false,
                        render: function (data, type, row, meta) {
                            @if(Auth::user()->name == 'webqlo')
                            return moment(data).format('D MMM YYYY (h:mm a)');
                            @else
                            return moment(data).format('D MMM YYYY');
                            @endif
                        }
                    }
                ]
            });

            $('#registration-datepicker, #customer-datepicker').datetimepicker({
                format: 'D MMM YYYY',
                useCurrent: false,
                ignoreReadonly: true,
                showTodayButton: true,
                showClear: true
            }).on('dp.change', function () {
                var dt, dtId = $(this).attr('id'), dateVal = $('#customer-datepicker').val();
                switch (dtId) {
                    case 'registration-datepicker':
                        dt = reservationTable;
                        break;

                    case 'customer-datepicker':
                        dt = customerTable;

                        if (dateVal) {
                            $('#export-text').text('This Day\'s Record');
                            $('#export-link').attr( 'href', '{{ route('export') }}/' + moment(dateVal).format('YYYY-MM-DD') );
                        } else {
                            $('#export-text').text('All Records');
                            $('#export-link').attr('href', '{{ route('export') }}');
                        }
                        break;        
                }
                dt.draw();
            });

            $('.input-daterange').datepicker({
                format: 'yyyy-mm-dd',
            });

            $('#save_btn').on('click', function (e) {
                e.preventDefault();
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                var start_date = $('#start_date').val();
                var end_date = $('#end_date').val();
                var start_date_time = start_date + 'T00:00:00+08:00';
                var end_date_time = end_date + 'T23:59:59+08:00';
                var start_html = new Date(start_date_time);
                var end_html = new Date(end_date_time);
                $.ajax({
                    type:'POST',
                    data: {_token: CSRF_TOKEN, start: start_date_time, end: end_date_time},
                    url: '{{ route('save-date') }}',
                    dataType: 'JSON',
                    success: function(data) {
                        alert(data.msg);
                        $('#start-time').html(moment(start_date_time).format('DD MMM YYYY, HH:mm'));
                        $('#end-time').html(moment(end_date_time).format('DD MMM YYYY, HH:mm'));
                    }
                });
            });

        });
    </script>
@endsection


<div class="modal fade" id="privacy_policy_new" role="dialog">
    <div class="modal-dialog modal-lg">               
      	<div class="modal-content" style="color:#555; border-radius:0;">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal">&times;</button>
               <h4 class="modal-title" style="text-align: center; font-weight:bold;">Privacy Policy</h4>
            </div>
       		<div class="modal-body">
				<div>
					<div class="cont1 clearfix">
						{{-- <h2>Privacy Policy</h2>
						<ul>
							<li><a href="#">1. APPLICATION</a></li>
							<li><a href="#">2. CONSENT</a></li>
							<li><a href="#">3. PURPOSE</a></li>
							<li><a href="#">4. WITHDRAWAL OF CONSENT, ACCESS & CORRECTION</a></li>
							<li><a href="#">5. CHILDREN</a></li>
							<li><a href="#">6. THIRD PARTY DISCLOSURE & TRANSFER</a></li>
							<li><a href="#">7. SECURITY & PROTECTION</a></li>
							<li><a href="#">8. RETENTION OF PERSONAL DATA</a></li>
							<li><a href="#">9. GOVERNING LAW</a></li>
							<li><a href="#">10. CONTACT US</a></li>
						</ul> --}}
						<p>Shiseido Malaysia Sdn. Bhd. (<strong>“Shiseido”</strong> or <strong>“We”</strong>) is committed to protecting
							your privacy and ensuring that your Personal Data is protected. For the purposes of this Privacy Policy,
							"Personal Data" means any personally identifiable data, whether true or not, about an individual who can be
							identified from that data.</p>
					</div>
					<div class="cont2">
						<div class="cont_box">
							<h3>1. APPLICATION</h3>
							<p>This Privacy Policy explains the types of Personal Data we collect and how we use, disclose, transfer,
								process and protect that information.</p>
							<p>We collect Personal Data through, but not limited to, the following means:</p>
							<div class="latin_listbox">
								<ul>
									<li>(a) When you shop on or browse [https://www.shiseido.com.my] (the <strong>“Website”</strong>);
									</li>
									<li>(b) When you shop in-store at our physical stores;</li>
									<li>(c) When you connect with us through social media or attend our marketing events; and</li>
									<li>(d) When you agree and consent to be a member of the Shiseido Membership, whether through
										physical or electronic means.
									</li>
								</ul>
							</div>
							<p>We may update this Privacy Policy from time to time by posting updated versions on the Website, and/or by
								sending an e-mail to you. Your continued membership, access to and/or use of the Website will be taken
								to be your agreement to, and acceptance of, all changes made in each updated version.<br>Please check
								back regularly for updated information on how we handle your Personal Data.</p>
						</div>
						<div class="cont_box">
							<h3>2. CONSENT</h3>
							<p>We do not collect, use or disclose your Personal Data without your consent (except where permitted and
								authorised by law). By providing your Personal Data to us, you hereby consent to us collecting, using,
								disclosing, transferring, and processing your Personal Data for the purposes set out in Section 3 of
								this Privacy Policy.</p>
							<p>The types of Personal Data we collect include, but are not limited to, your: (a) first name and family
								name; (b) home address; (c) date of birth; (d) email address; and, only if appropriate, your (e) user
								name and password; (f) billing and delivery address; (g) personal identification number; and (h) other
								information as may be reasonably required for us to provide you with the Services as defined in Section
								3 below.</p>
						</div>
						<div class="cont_box">
							<h3>3. PURPOSE</h3>
							<p>We collect, use, disclose, transfer and process your Personal Data for the purpose of providing services.
								These services include, but are not limited to:</p>
							<div class="latin_listbox">
								<ul>
									<li>1. providing you with information on products and campaigns from us, Shiseido Group and our
										third party business partners via email, SMS, and post (where we have your express consent);
									</li>
									<li>2. allowing you to purchase products and services offered for sale via the Website;</li>
									<li>3. facilitating your transactions with us;</li>
									<li>4. sending you product samples and/or products;</li>
									<li>5. keeping you informed of updates, changes, and developments relating to us and our Services;
									</li>
									<li>6. notifying you about important changes to this Privacy Policy, and to our other policies or
										services;
									</li>
									<li>7. providing you with personalized consultations;</li>
									<li>8. responding to queries or feedback from you;</li>
									<li>9. maintaining and operating the Website;</li>
									<li>10. managing our administrative and business operations;</li>
									<li>11. engaging third party business partners and data processors to perform certain aspects of the
										Services;
									</li>
									<li>12. performing customer profiling, market analysis, and research to improve our product and
										service offerings to you;
									</li>
									<li>13. preventing, detecting and investigating crime and analysing and managing commercial risks;
										and
									</li>
									<li>14. other purposes which are reasonably related to the above.</li>
								</ul>
							</div>
							<p>(collectively, the <strong>"Services"</strong>)</p>
						</div>
						<div class="cont_box">
							<h3>4. WITHDRAWAL OF CONSENT, ACCESS & CORRECTION</h3>
							<p>If you wish to withdraw your consent to receive information on new products and campaigns, or any other
								Services, you may do so by:</p>
							<div class="latin_listbox">
								<ul>
									<li>1. unsubscribing from our Website;</li>
									<li>2. clicking the “Unsubscribe" link in the email(s) we send to you;</li>
									<li>3. contacting our Data Protection Officer at the email address below; or</li>
									<li>4. writing to us at the address below.</li>
								</ul>
							</div>
							<p>Please note that if you choose not to provide us with certain Personal Data, or to withdraw your consent
								to our use, disclosure, transfer and/or processing of your Personal Data, we may not be able to provide
								you with some or all of the Services.</p>
							<p>We will ensure that the Personal Data in our possession is accurate and complete to the best of our
								knowledge.</p>
							<p>You have a right to request for access and correction of your Personal Data. If you would like assistance
								in accessing and/or correcting your Personal Data, please contact our Data Protection Officer at the
								email address below. We will get back to you within 21 days.</p>
						</div>
						<div class="cont_box">
							<h3>5. CHILDREN</h3>
							<p>This Website is directed toward and designed for use by persons aged 16 or older. We do not intend to
								collect Personal Data from children under 16 years of age, except on some sites specifically directed to
								children.</p>
							<p>We protect the Personal Data of children along with the necessary parental consent in the same manner as
								it protects the Personal Data of adults.</p>
						</div>
						<div class="cont_box">
							<h3>6. THIRD PARTY DISCLOSURE & TRANSFER</h3>
							<p>We do not disclose or transfer your Personal Data to third parties unless we have clearly asked for and
								obtained your consent to do so (except where permitted and authorised by law).</p>
							<p>The Personal Data which you provide to us may be stored, processed, transferred between, and accessed
								from servers located in the United States and other countries which have laws and regulations that may
								not guarantee the same level of protection of Personal Data as Malaysia. However, we will take
								reasonable steps to ensure that your Personal Data is handled in accordance with this Privacy Policy,
								regardless of where your Personal Data is stored or accessed from.</p>
							<div class="num_listbox">
								<dl>
									<dt>6.1 Disclosure to affiliated companies in the Shiseido Group</dt>
									<dd>The Shiseido Group comprises a number of affiliated companies and legal entities located both
										within and outside Malaysia. We may disclose, where appropriate and to the extent necessary,
										your Personal Data to such affiliated companies and legal entities for the purposes of corporate
										reporting, market research and analysis, customer relationship management and other related
										legal and business purposes. Please note that we provide our affiliated companies and legal
										entities with only the Personal Data they need for such business and legal purposes, and we
										require that they protect such Personal Data in accordance with the applicable laws and
										regulations and this Privacy Policy, and not use it for any other purpose.
									</dd>
									<dt>6.2 Disclosure to third party business partners</dt>
									<dd>We rely on third party business partners to perform a variety of services on our behalf. In so
										doing, Shiseido may let service providers, located both within and outside Malaysia, use your
										Personal Data for the marketing and promotion of products, services or events that may be of
										interest to you, for market research and analysis, for customer relationship management, and for
										the fulfilment of your orders for products and services purchased via the Website. Please note
										that we provide our third party business partners with only the Personal Data they need to
										perform their services and we require that they protect such Personal Data in accordance with
										the applicable laws and regulations and this Privacy Policy, and not use it for any other
										purpose.
									</dd>
									<dt>6.3 Disclosure to third party data processors</dt>
									<dd>We may use third party service providers, located both within and outside Malaysia, to help us
										maintain and operate the Website, or for other reasons related to the operation of the Website
										and Shiseido’s business, and they may receive your Personal Data for these purposes. We only
										provide them the Personal Data they need to provide these services on our behalf. We require
										these companies to protect the Personal Data in accordance with the applicable laws and
										regulations and this Privacy Policy, and to not use the information for any other purpose.
									</dd>
									<dt>6.4 Other disclosure</dt>
									<dd>We may use and disclose your Personal Data to perform your instructions and, as relevant, (a)
										comply with legislative and regulatory requirements; (b) protect or defend rights or properties
										of customers and employees of Shiseido; and/or (c) take emergency measures for the purpose of
										securing the safety of customers, Shiseido, or the general public. We may also disclose and
										transfer our personal data to other service providers and/or third parties (which may be located
										both within and outside Malaysia) in the context of a merger, acquisition or any other corporate
										exercise involving Shiseido.
									</dd>
								</dl>
							</div>
						</div>
						<div class="cont_box">
							<h3>7. SECURITY & PROTECTION</h3>
							<p>We maintain strict procedures, standards, and security arrangements to protect Personal Data in our
								possession or under our control. Upon receipt of your Personal Data, whether through physical or
								electronic means of collection, we will make the necessary security arrangements to protect such
								Personal Data as are reasonable and appropriate in the circumstances. Such arrangements may comprise
								administrative measures, physical measures, technical measures, or a combination of such measures.</p>
							<p>When disclosing or transferring your Personal Data over the internet, we take all reasonable care to
								prevent unauthorised access to your Personal Data. However, no data transmission over the internet can
								be guaranteed as fully secure and you acknowledge that you submit information over the internet at your
								own risk.</p>
						</div>
						<div class="cont_box">
							<h3>8. RETENTION OF PERSONAL DATA</h3>
							<p>We will not retain your Personal Data for any period of time longer than is necessary to serve the
								purposes set out in this Privacy Policy and any valid business or legal purposes. After this period of
								time, we will destroy or anonymise any documents containing your Personal Data in a safe and secure
								manner.</p>
						</div>
						<div class="cont_box">
							<h3>9. GOVERNING LAW</h3>
							<p>This Privacy Policy is governed by Malaysia law.</p>
						</div>
						<div class="cont_box">
							<h3>10. CONTACT US</h3>
							<p>If you would like to access or correct any Personal Data which you have provided to us, submit a
								complaint, or have any queries about your Personal Data, please contact our Data Protection Officer by
								contacting us at <a
										href="mailto:shiseidocamellia@shiseido.com.my">shiseidocamellia@shiseido.com.my</a>.
								Alternatively, you may write to us at:<br>
								Shiseido Malaysia Sdn.Bhd.<br>
								Unit 7-03, Level 7, Menara UAC, No.12, Jalan PJU 7/5, Mutiara Damansara, 47800 Petaling Jaya, Selangor
								Darul Ehsan,Malaysia<br>
								Tel:60-3-7719-1888
							</p>
						</div>
						<div class="cont_box">
							<p>Date: 20 July 2018 </p>
						</div>
					</div>
				</div>
				<!-- /#main -->
			</div>
		</div>
	</div>
</div>

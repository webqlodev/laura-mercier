<div id="header" class="row">
    <div class="col-md-12 text-center pt-1">
        <a href="{{url('/')}}">
            <img class="logo-image" src="{{ asset('img/LauraMercier_logo.png') }}"/>
        </a>
    </div>
    {{--<div class="col-md-8">
        <ul class="nav navbar-nav">
            <li><a class="menu" atr="home">Home</a></li>
            <li><a class="menu" atr="taptapwaso">Feeling WASO Good</a></li>
            <li><a class="menu" atr="product">Products</a></li>
            <li><a class="menu" atr="ingredient">Ingredients</a></li>
        </ul>
    </div>--}}
</div> <!-- header -->

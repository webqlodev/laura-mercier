<div id="footer_section" class="container-fluid">
    <div class="row text-center">
        <div class="col-xs-12 upperfont pb-2">
            <div class="gotham-bold-24pt">find us at</div>
        </div>
        <div class="col-xs-12 upperfont line-spacing pb-2">
            <div class="gotham-bold-18pt">pavilion kuala lumpur</div>
            <div class="gotham-book-16pt">Lot 3.01.01, level 3<br>03-21100184</div>
        </div>
        <div class="col-xs-12 upperfont line-spacing pb-2">
            <div class="gotham-bold-18pt">1utama shopping centre</div>
            <div class="gotham-book-16pt">Lot g143, ground floor<br>03-77316213</div>
        </div>
        <div class="col-xs-12 upperfont line-spacing pb-2">
            <div class="gotham-bold-18pt">sunway pyramid shopping mall</div>
            <div class="gotham-book-16pt">Lot g1. 128 a ground floor<br>03-56112371</div>
        </div>
        <div class="col-xs-12 upperfont line-spacing pb-2">
            <div class="gotham-bold-18pt">gurney plaza, penang</div>
            <div class="gotham-book-16pt">Lot 170, g-k15, ground floor<br>04-2170234</div>
        </div>
        <div class="col-xs-12 upperfont line-spacing">
            <div class="gotham-bold-18pt">aeon tebrau city, <br>johor bahru</div>
            <div class="gotham-book-16pt">Lot g20, ground floor<br>07-3572627</div>
        </div>
        <div class="visible-xs-block visible-sm-block col-xs-12 upperfont">
            <div class="fb_btn">
                <i class="fab fa-facebook-f fb_logo"></i>
                <a href="{{ route('redirect', ['location' => 'facebook']) }}" target="_blank">Follow Us</a>
            </div>   
        </div>
        <div class="visible-xs-block visible-sm-block col-xs-12 col-md-6 text-center">
            <p class="small">
                Copyright by Laura Mercier Malaysia
            </p>
        </div>
        <div class="col-xs-12 visible-md-block visible-lg-block text-center pt-1">
            <div class="row">
                <div id="fbus" class="col-xs-6 text-left">
                    <a href="{{ route('redirect', ['location' => 'facebook']) }}" target="_blank">
                        <i class="fab fa-facebook-square"></i>
                        <span class="fb-font">Follow Us</span>
                    </a>
                </div>
                <div class="col-xs-6 text-right">
                    <p class="small">
                        Copyright by Laura Mercier Malaysia
                    </p>
                </div>              
            </div>
        </div>     
    </div>
</div>

<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    
    <title>{{ config('app.name', 'Laura Mercier Cosmetics Malaysia | #CityOfLights') }}</title>
    <meta name="description" content="Laura Mercier Holiday 2018 Collection themed City of Lights captures the vivid colours and glamour of the holidays in the city imbued in an alluring festive glow."/>
    <meta name="keywords" content="lauramercier, malaysia, cityoflights, face, palette, skincare, brush, eye, lip"/>
    {{--<link rel="icon" type="image/png" href="{{url('img/black-favicon.png')}}">--}}

    <!-- SEO Keywords -->
    <meta property="og:url"           content="https://www.lauramercier.com/" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Laura Mercier | #CityOfLights" />
    <meta property="og:description"   content="Laura Mercier Holiday 2018 Collection themed City of Lights captures the vivid colours and glamour of the holidays in the city imbued in an alluring festive glow." />
    {{--<meta property="og:image"         content="{{url('img/waso-fb-share.jpg')}}" />--}}

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" media="all" />
    <link rel="stylesheet" href="//use.fontawesome.com/releases/v5.4.1/css/all.css">
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('css/bootstrap-responsive-tabs.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/privacy.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    @yield('css')
</head>
<body>
    @yield('content')
    <!-- Scripts -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="{{ asset('js/jquery.bootstrap-responsive-tabs.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/1000hz-bootstrap-validator/0.11.9/validator.min.js" integrity="sha256-dHf/YjH1A4tewEsKUSmNnV05DDbfGN3g7NMq86xgGh8=" crossorigin="anonymous"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.min.js"></script>
    <script src="{{ asset('js/script.js') }}"></script>
    <script type="text/javascript" async src="https://platform.twitter.com/widgets.js"></script>
    <script src="{{ asset('js/datatables.min.js') }}"></script>
    <script src="{{ asset('js/phaser.min.js') }}"></script>
	<script src="{{ asset('js/Boot.js') }}"></script>
	<script src="{{ asset('js/Preloader.js') }}"></script>
	<script src="{{ asset('js/MainMenu.js') }}"></script>
	<script src="{{ asset('js/Game.js') }}"></script>
    @yield('script')
    @include('tnc')
    @include('privacy-laura')
    @include('consent')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-127961542-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-127961542-1');
    </script>

</body>
</html>

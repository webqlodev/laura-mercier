@extends('layouts.app')

@section('content')
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <img src="{{ asset('img/LauraMercier_logo.png') }}" />
        </div>

        <ul class="nav navbar-nav navbar-right">
            <li>
                <form method="post" action="{{ route('logout') }}">
                    {{ csrf_field() }}
                    <button type="button">Logout</button>
                </form>
            </li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1>New Player Count by Date</h1>
                </div>

                <table class="table">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>New Player</th>
                        </tr>
                    </thead>

                    <tbody></tbody>
                </table>
            </div>
        </div>

        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1>Click Count Totals</h1>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Facebook</th>
                                <th>YouTube</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <h1>Player List and Details</h1>

    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Name</th>
                <th>Email</th>
                <th>Phone</th>
                <th>Score</th>
                <th>Create Date</th>
                <th>Last Update Date</th>
            </tr>
        </thead>

        <tbody></tbody>
    </table>
</div>
@endsection

@section('script')
<script>
$(function () {

});
</script>
@endsection

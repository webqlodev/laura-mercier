<style>
	.sheading{text-decoration:underline;}
    .modal-body {padding:30px!important;}
</style>
<div class="modal fade" id="consent" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content" style="color:#555; border-radius:0;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" style="text-align: center; font-weight:bold;">Consent</h4>
		    </div>
			<div class="modal-body">
				<p>
					You hereby agree and consent to Shiseido Malaysia Sdn. Bhd. ("Shiseido") collecting, using, disclosing, transferring, and processing your personal data, in accordance with Shiseido's Privacy Policy (accessible online at <a href="https://www.shiseido.com.my/privacypolicy" target="_blank">https://www.shiseido.com.my/privacypolicy</a>).
					<br />
					By providing your personal data to us, you agree and consent to; 
				</p>
				<ol type="a">
					<li>be included in our databases for our sales and marketing opportunities, including for establishing an account for checkout purposes if applicable; and</li>
					<li>Shiseido providing, or continuing to provide, you with the Services described in Section 3 of Shiseido's Privacy Policy.</li>
				</ol>
				<p>
					You specifically agree and consent to the transfer of your personal data to affiliated companies within the Shiseido Group and to third party service providers hired by Shiseido to process such data, both within and outside of Malaysia, in accordance with Shiseido's Privacy Policy.
					<br />
					You would also like to receive information on products and campaigns from Shiseido, Shiseido Group and our third party business partners via email, SMS and post. 
				</p>
				<ul>
					<li>I consent to receiving such information as stated above.</li>
					<li>I consent to Shiseido's use of my skin-related information and health-related information in accordance with Shiseido's Privacy Policy.</li>
				</ul>
			</div><!-- modal body -->
		</div>
	</div>
</div> 

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('main');
Route::post('/', 'HomeController@submit');
Route::get('/thank-you/{code}', 'HomeController@thankYou')->name('thank-you');
Route::get('/redirect/{location}', 'HomeController@redirect')->name('redirect');
Route::get('/end', function() { return view('end');});
/**admin**/
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/login', 'Auth\LoginController@login');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/admin', 'AdminController@index')->name('admin');
Route::post('registration', 'AdminController@getRegistration')->name('registration');
Route::post('/save-date', 'AdminController@saveDate')->name('save-date');
Route::post('customers', 'AdminController@getCustomers')->name('customers');
Route::get('export/{date?}', 'AdminController@exportExcel')->name('export');
Route::get('/generate-code/{amount}', 'AdminController@generateUniqueCodes')->name('generate-code');
Route::get('/export-code-list', 'AdminController@exportCodeList')->name('export-code-list');
Auth::routes();
